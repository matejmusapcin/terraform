variable "namespace" {
  description = "The project namespace to use for unique resource naming"
  default     = "playground"
  type        = string
}

variable "region" {
  description = "AWS region"
  default     = "eu-south-1"
  type        = string
}

variable "aws_account_id" {
  description = "AWS account ID"
  default     = "692634189512"
  type        = string
}